const Server = require("./src/server");
const router = require("./src/routers/index");

const mongoose = require('mongoose');
const { MONGO_DATABASE, MONGO_HOST} = require("./src/config/index");


server = new Server(router());

mongoose.connect(`mongodb://${MONGO_HOST}/${MONGO_DATABASE}`, 
{
  useNewUrlParser: true, 
  useUnifiedTopology: true,
  useCreateIndex: true
})
.then(() => {
  server.start();
})
.catch(() => {
  console.log("Error connecting to database");
})
