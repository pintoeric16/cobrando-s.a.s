var mv = require('mv');
const formidable = require('formidable');


 /**
   * 
   * @param {array} json the array of objects for rename keys
   * @param {Object} keys the new keys for objects
   */
  async function convertKeys (json, keys) {
    for await (let obj of json) {
      await Object.keys(obj).map(key => renameKey(obj, key, keys[key]));
    }
  }

  /**
   * for rename a specific key of object
   * @param {Object} obj object for rename the key
   * @param {string} oldKey old key name
   * @param {string} newKey new key name
   */
  function renameKey(obj, oldKey, newKey) {
    Object.defineProperty(obj, newKey, Object.getOwnPropertyDescriptor(obj, oldKey));
    delete obj[oldKey];
  }

  /**
   * for move the received excel to a temp folder on server
   * @param {string} oldPath path of excel received
   * @param {string} newPath path temp of excel
   */
  function mvFile (oldPath, newPath) {
    return new Promise((resolve, reject) => {
      mv(oldPath, newPath, (err) => {
        if (err) reject('Error moving file to server');
        resolve();
      });
    });
  }

  /**
   * for parse excel file received in form
   * @param {Object} req require object of express
   */
  function parseExcel (req) {
    return new Promise((resolve, reject) => {
      const form = new formidable.IncomingForm();
      form.parse(req, async (err, fields, files) => {
        if (err) reject('Error parse file');
        resolve ({
          oldPath: files.excel.path,
          newPath: __dirname + "/../tmp/" + files.excel.name
        });
      });
    });
  }

  module.exports = {
    convertKeys,
    mvFile,
    parseExcel
  }