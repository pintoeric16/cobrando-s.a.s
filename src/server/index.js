const express = require("express");
var bodyParser = require("body-parser");
const {PORT} = require("../config/index");

class Server {

  constructor(router){
    this.app = express().use(router);
    this.app.use(bodyParser.urlencoded({extended: true}));
  }

  start(){
    return new Promise(resolve => {
      this.app.listen(PORT, ()=> {
        console.log('app runing on: http://localhost:' + PORT);
      });

      resolve();
    });
  }
}


module.exports = Server;