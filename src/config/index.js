if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

module.exports = {
  PORT: process.env.PORT,
  MONGO_HOST: process.env.MONGO_HOST,
  MONGO_DATABASE: process.env.MONGO_DATABASE,
  JWT_SECRET: '(abCDefGHijKLmnOPqrSTuvWXyz)'
}