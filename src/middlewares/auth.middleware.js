const  JWT = require("jsonwebtoken")
const {JWT_SECRET} = require("../config/index");
const express = require("express");
const protecteRoute = express.Router(); 

protecteRoute.use((req, res, next) => {
  const token = req.headers["authorization"];
  if (token) {
    JWT.verify(token, JWT_SECRET, (error, tokenDecode) => {
      if (error) {
        return res.json({error: "Invalid token"})
      }else{
        req.tokenDecode = tokenDecode;
        next();
      }
    })
  }else{
    return res.json({error: "Not provided token"});
  }
}); 


module.exports = protecteRoute;