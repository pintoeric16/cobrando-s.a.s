const mongoose = require('mongoose');
const { Schema } = mongoose;
const uniqueValidator = require('mongoose-unique-validator');
const { compareSync, hashSync, genSaltSync } = require('bcryptjs');

const userSchema = new Schema({
  email: { type: String, required: true, unique: true},
  password: { type: String, required: true},  
},
{
  timestamps: true
});

userSchema.pre('save', async function(next){
  const user = this;

  if(!user.isModified('password')){
    return next()
  }

  const salt = genSaltSync(10);
  const hanshpassword = hashSync(user.password, salt);
  user.password = hanshpassword;

  return next();

});

userSchema.methods.toJSON = function() {
  let user = this.toObject();
  delete user.password; 
  delete user.__v;
  return user;
};

userSchema.methods.comparePasswords = function(password) {
  return compareSync(password, this.password);
};

userSchema.plugin(uniqueValidator);

module.exports = mongoose.model('user', userSchema);