const { UserModel } = require("../models/index");

class UserController {

	constructor() { }

	async getUserByEmail(email) {
    const user =  await UserModel.findOne({email});
    if (!user) {
      const error = new Error();
			error.status = 404;
			error.message = "User not found";
			return {error};
    }
    return user;
  }
}

module.exports = UserController;