const { generateToken } = require("../helpers/index");
const { UserModel } = require("../models/index");
const UserController = require("./user.controller");


class AuthController {

	constructor() {
		this.userController = new UserController();
	 }

	async signup(body) {
		try {
			await UserModel.create(body);
		} catch (errors) {
			const error = new Error();
			error.status = 412;
			error.message = errors.message;
			return {error};
		}
		return {data: "User created successfully"};
	}

	async singin (credential) {
		const {email, password } = credential;
		if (!email || !password) {
			return {error: "Email and password are required"}
		}
		const existUser = await this.userController.getUserByEmail(email);
		 if (existUser.error) {
			 return existUser;
		 }
		 const validPassword = existUser.comparePasswords(password);
		 if (!validPassword) {
      return {error: "Incorrect password" };
		}
		const token = generateToken(existUser);


		return { data: {token, user: existUser }};
	}
}

module.exports = AuthController;