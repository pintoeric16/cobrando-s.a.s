const excelToJson = require('convert-excel-to-json');
const mongoose = require("mongoose");
const { parseExcel, mvFile, convertKeys } = require("../helpers/index");


class ExcelController {

	constructor() { }

	async processingExcel(req) {
    const paths = await parseExcel(req);
    await mvFile(paths.oldPath, paths.newPath);
    const result = excelToJson({
        sourceFile: paths.newPath,
        sheets: ['Hoja1']
    });
    const headers = result.Hoja1.shift();
    const json = result.Hoja1;
    await convertKeys(json, headers);
    for await (let obj of json) {
      await mongoose.connection.collection('excel').insertOne(obj);
    }
    return json;
  }

  async getTable(){
    const excelCollection = mongoose.connection.collection('excel'); 
    let headers = await excelCollection.aggregate([
      {
        $project: {
            arrayOfKeyValue: {
                $objectToArray: "$$ROOT"
            }
        }
      },
      {
          $unwind: '$arrayOfKeyValue'
      },
      {
          $group: {
              _id: null,
              allKeys: {
                  $addToSet: '$arrayOfKeyValue.k'
              }
          }
      },
      {
          $project: {
              _id: 0
          }
      }
    ]).toArray();

    headers = headers[0].allKeys;
    const allDocs = await excelCollection.find().toArray();
    return { headers, allDocs };
  }
}

module.exports = ExcelController;


