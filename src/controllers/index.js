const AuthController = require("./auth.controller");
const UserController = require("./user.controller");
const ExcelController = require("./excel.controller");


module.exports = {
  AuthController,
  UserController,
  ExcelController
}