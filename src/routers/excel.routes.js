const { Router } = require("express");
const mongoose = require("mongoose");
const { ExcelController } = require("../controllers/index");

module.exports = () => {
  const router = Router();
  const excelController = new ExcelController();

  router.get('/', async (req, res) => {
    excelController.getTable()
    .then(data => res.json(data))
    .catch(err => res.json({error: 'failed get data table '}))
  })


  router.post('/', async (req, res) => {
    excelController.processingExcel(req)
    .then(data => res.json(data))
    .catch(err => res.json({error: 'load failed'}))
  });

  return router;
}