const express = require("express");
const cors = require("cors");
const helmet = require("helmet");
const compression = require("compression");
const { protecteRoute } = require("../middlewares/index")
const {
  AuthRoutes,
  UserRoutes,
  ExcelRoutes
} = require("./index.routes")

module.exports = () => {
  const router = express.Router();
  const apiRoutes = express.Router();

  apiRoutes
    .use(express.json())
    .use(cors())
    .use(helmet())
    .use(compression());

    apiRoutes.use("/auth", AuthRoutes());
    apiRoutes.use("/user", UserRoutes());
    apiRoutes.use("/excel",protecteRoute, ExcelRoutes());

    router.use("/api", apiRoutes);

    return router;

}