const AuthRoutes = require("./auth.routes");
const UserRoutes = require("./user.routes");
const ExcelRoutes = require("./excel.routes");

module.exports = {
  AuthRoutes,
  UserRoutes,
  ExcelRoutes
}