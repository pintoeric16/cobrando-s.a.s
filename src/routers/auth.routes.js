const { Router } = require("express");
const { AuthController } = require("../controllers/index");
const { create } = require("../models/user.model");

module.exports = () => {
  const router = Router();
  const authController = new AuthController();

  router.post("/singup", (req, res) => {
    const {body} = req;
    authController.signup(body)
    .then(data => {
      res.json(data)
    })
    .catch(error => {
      res.json(error)
    })
  });


  router.post("/singin", (req, res) => {
    const {body} = req;
    authController.singin(body)
    .then(data=> {
      res.json(data);
    })
  });

  return router;
}
