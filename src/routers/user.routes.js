const { Router } = require("express");
const { UserController } = require("../controllers/index")

module.exports = () => {
  const router = Router();
  const userController = new UserController();

  router.post("/search", (req, res) => {
    const {body} = req;
    const  {email}  = body
    userController.getUserByEmail(email)
    .then(data => {
      res.json(data);
    }).catch(error => {
      res.json(error);
    })
  });

  return router;
}
